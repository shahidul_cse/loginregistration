<?php
require_once './view/header.php';
require_once './view/menu.php';
require_once './file/function.php';
$obj = new LoginRegistration();
?>
<div class="maincontent">
    <div class="login_header">
        <h2>User registration form</h2>
        <p>
            <?php
                if($_SERVER['REQUEST_METHOD']=='POST') {
                   $user_name = $_POST['user_name'];
                   $password = $_POST['password'];
                   $email = $_POST['email'];
                   $cuntry = $_POST['cuntry'];
                   $city = $_POST['city'];
                   
                   if(empty($user_name) or empty($password) or empty($email) or empty($cuntry) or empty($city)) {
                       echo 'Fieled mus not be empty';
                   } else {
                       $password = md5($password);
                      $obj->resisterUser($user_name,$password,$email,$cuntry,$city);
                      if($obj){
                          echo "<span>Register done <a href='login.php'>Click here</a> for login</span>";
                      }  else {
                          echo 'email already exist';
                      }
                   }
                }
            ?>
        </p>
    </div>
    <div class="login_form">
        <form action="" method="post">
            <table>
                <tr>
                    <td>User Name: </td>
                    <td><input type="text" name="user_name"></td>
                </tr>
                <tr>
                    <td>Password: </td>
                    <td><input type="password" name="password"></td>
                </tr>
                <tr>
                    <td>Email: </td>
                    <td><input type="email" name="email"></td>
                </tr>
                <tr>
                    <td>Country: </td>
                    <td><input type="text" name="cuntry"></td>
                </tr>
                <tr>
                    <td>City: </td>
                    <td><input type="text" name="city"></td>
                </tr>
                <tr>
                    <td colspan="2">                        
                        <input type="reset" name="reset" value="Reset">
                        <input type="submit" name="register" value="Register">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>


<?php require_once './view/footer.php';?>

